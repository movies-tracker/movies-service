package com.vkainth.movies.main;

import com.vkainth.movies.main.controller.Controller;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Disabled("Integration tests are disabled")
public class SmokeTest {
    static {
        System.setProperty("MONGO_HOST", "localhost");
        System.setProperty("MONGO_PORT", "34000");
        System.setProperty("MONGO_USER", "root");
        System.setProperty("MONGO_PW", "root");
        System.setProperty("MONGO_DB", "movies_tracker");
    }
    @Autowired
    private Controller controller;

    @Test
    public void contextLoads() throws Exception {
        Assertions.assertThat(controller).isNotNull();
    }
}