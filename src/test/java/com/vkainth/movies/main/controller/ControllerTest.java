package com.vkainth.movies.main.controller;

import com.vkainth.movies.main.model.Movie;
import com.vkainth.movies.main.repository.MovieRepository;
import com.vkainth.movies.main.service.MovieService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.ArrayList;

@ExtendWith(SpringExtension.class)
@WebMvcTest(Controller.class)
class ControllerTest {
    @MockBean
    private MovieService movieService;
    @MockBean
    private MovieRepository movieRepository;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        /*
         * { "Title": "Batman Begins", "Year": "2005", "Rated": "PG-13", "Released":
         * "15 Jun 2005", "Runtime": "140 min", "Genre": "Action, Adventure",
         * "Director": "Christopher Nolan", "Writer":
         * "Bob Kane (characters), David S. Goyer (story), Christopher Nolan (screenplay), David S. Goyer (screenplay)"
         * , "Actors": "Christian Bale, Michael Caine, Liam Neeson, Katie Holmes",
         * "Plot":
         * "After training with his mentor, Batman begins his fight to free crime-ridden Gotham City from corruption."
         * , "Language": "English, Mandarin", "Country": "USA, UK", "Awards":
         * "Nominated for 1 Oscar. Another 14 wins & 72 nominations.", "Poster":
         * "https://m.media-amazon.com/images/M/MV5BZmUwNGU2ZmItMmRiNC00MjhlLTg5YWUtODMyNzkxODYzMmZlXkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_SX300.jpg",
         * "Ratings": [ { "Source": "Internet Movie Database", "Value": "8.2/10" }, {
         * "Source": "Rotten Tomatoes", "Value": "84%" }, { "Source": "Metacritic",
         * "Value": "70/100" } ], "Metascore": "70", "imdbRating": "8.2", "imdbVotes":
         * "1,267,031", "imdbID": "tt0372784", "Type": "movie", "DVD": "18 Oct 2005",
         * "BoxOffice": "$204,100,000", "Production": "Warner Bros. Pictures",
         * "Website": "N/A", "Response": "True" }
         */
        String name = "Batman Begins";
        String year = "2005";
        String rating = "PG-13";
        String director = "Christopher Nolan";
        String plot = "After training with his mentor, Batman begins his fight to free crime-ridden Gotham City from corruption.";
        String posterUrl = "https://m.media-amazon.com/images/M/MV5BZmUwNGU2ZmItMmRiNC00MjhlLTg5YWUtODMyNzkxODYzMmZlXkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_SX300.jpg";
        Movie movie = new Movie();
        movie.setTitle(name);
        movie.setPlot(plot);
        movie.setDirector(director);
        movie.setPosterUrl(posterUrl);
        movie.setYear(year);
        movie.setRating(rating);

        List<Movie> movies = new ArrayList<>();
        movies.add(movie);

        Mockito.when(movieService.findAll()).thenReturn(movies);
    }

    @Test
    void whenGetAllMoviesIsReachedShouldReturnAllMovies() throws Exception {
        String expected = "[{\"id\":null,\"title\":\"Batman Begins\",\"plot\":\"After training with his mentor, Batman begins his fight to free crime-ridden Gotham City from corruption.\",\"posterUrl\":\"https://m.media-amazon.com/images/M/MV5BZmUwNGU2ZmItMmRiNC00MjhlLTg5YWUtODMyNzkxODYzMmZlXkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_SX300.jpg\",\"year\":\"2005\",\"imdbRating\":null,\"imdbVotes\":null,\"imdbID\":null,\"genre\":null,\"rating\":\"PG-13\",\"director\":\"Christopher Nolan\",\"country\":null,\"awards\":null}]";
        mockMvc.perform(MockMvcRequestBuilders.get("/movies").contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().string(expected));
    }
}