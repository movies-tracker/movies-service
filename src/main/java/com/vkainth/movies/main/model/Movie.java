package com.vkainth.movies.main.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "movies")
public class Movie {
    @Id
    private ObjectId id;
    private String title;
    private String plot;
    private String posterUrl;
    private String year;
    private String imdbRating;
    private String imdbVotes;
    private String imdbID;
    private String genre;
    private String rating;
    private String director;
    private String country;
    private String awards;
}