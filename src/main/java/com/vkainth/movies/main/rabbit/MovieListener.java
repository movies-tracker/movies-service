package com.vkainth.movies.main.rabbit;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vkainth.movies.main.model.Movie;
import com.vkainth.movies.main.service.MovieService;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class MovieListener {
    @Autowired
    private MovieService movieService;

    @RabbitListener(queues = "${movie.rabbitmq.queue}")
    public void receiveMovie(String stringMovie) {
        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            Movie movie = mapper.readValue(stringMovie, Movie.class);
            log.info("Saving movie with ID: {}", movie.getImdbID());
            movieService.save(movie);
        } catch (JsonProcessingException ex) {
            log.error("Unable to parse movie: {}", stringMovie);
            log.error("{}", ex);
        }
    }
}