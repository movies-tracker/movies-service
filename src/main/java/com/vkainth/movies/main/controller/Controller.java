package com.vkainth.movies.main.controller;

import com.vkainth.movies.main.model.Movie;
import com.vkainth.movies.main.service.MovieService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;

import java.util.List;

@RestController
@Log4j2
public class Controller {
    
    @Autowired
    private MovieService movieService;

    @GetMapping("/")
    public String getHello() {
        log.info("Home Page Requested");
        return "Hello to Main Movies Service. You're looking for /movie";
    }

    @GetMapping("/movies")
    public List<Movie> getAllMovies() {
        log.info("All movies requested");
        return movieService.findAll();
    }

    @GetMapping("/movies/imdb/{id}")
    public Movie getMovieByImdb(@PathVariable("id") String id) {
        log.info("Movie for IMDB Id: {} requested", id);
        return movieService.findByImdbId(id);
    }

    @GetMapping("/movies/title/{title}")
    public List<Movie> getMoviesByTitle(@PathVariable("title") String title) {
        // Supports regex matching so the title doesn't have to exactly match
        log.info("Movie for Title: {} requested", title);
        return movieService.findAllByTitle(title);
    }

    @PostMapping("/movies")
    public Movie createMovie(@RequestBody Movie movie) {
        log.info("Creating movie for Title: {}", movie.getTitle());
        return movieService.save(movie);
    }

}