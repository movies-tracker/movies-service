package com.vkainth.movies.main.repository;

import com.vkainth.movies.main.model.Movie;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface MovieRepository extends MongoRepository<Movie, String> {

    @Query("{'imdbID': ?0}")
    Movie findOneByImdbId(String imdbId);

    @Query("{'title': {$regex: ?0}}")
    List<Movie> findAllByTitle(String title);

}