package com.vkainth.movies.main.service;

import com.vkainth.movies.main.model.Movie;
import com.vkainth.movies.main.repository.MovieRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;

import java.util.*;

@Service
@Log4j2
public class MovieService {
    @Autowired
    private MovieRepository movieRepository;

    public List<Movie> findAll() {
        return movieRepository.findAll();
    }

    public Movie save(Movie movie) {
        // Lookup for the movie first
        Movie toFind = new Movie();
        toFind.setTitle(movie.getTitle());
        Example<Movie> movieExample = Example.of(toFind);
        Optional<Movie> isFound = movieRepository.findOne(movieExample);
        if (isFound.isPresent()) {
            log.info("Movie Title: {} exists. Not creating", movie.getTitle());
            return isFound.get();
        }
        return movieRepository.save(movie);
    }

    public List<Movie> findAllByTitle(String name) {
        log.info("Requesting movie title: {}", name);
        return movieRepository.findAllByTitle(name);
    }

    public Movie findByImdbId(String imdbId) {
        log.info("Requesting movie IMDB ID: {}", imdbId);
        return movieRepository.findOneByImdbId(imdbId);
    }
}