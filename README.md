[![pipeline status](https://gitlab.com/movies-tracker/movies-service/badges/master/pipeline.svg)](https://gitlab.com/movies-tracker/movies-service/-/commits/master)


# Movies Microservice

The Movies microservice for the Movie Tracker Web Application.

## Introduction

This microservice is intended as the main mover for Retrieving and Creating movies.
