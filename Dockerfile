FROM openjdk:14-jdk-alpine

RUN addgroup -S spring && adduser -S spring -G spring

USER spring:spring

EXPOSE 8080

ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} app.jar

ENTRYPOINT [ "java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "app.jar" ]